﻿using System.Collections.Generic;
using UnityEngine;
using System;

public enum GameState
{
    idle,
    walk,
    battle,
    talking,
    loading
}

public class LevelController : MonoBehaviour
{
    public const float MoveSpeed = 5f;
    public const float AnimationCooldown = .4f;
    float currentAnimationCooldown = 0f;

    public static LevelController instance { get; private set; } // Singleton implementation
    public GameState State { get; private set; } = GameState.idle;

    public MapController mapController;

    Dictionary<MapGenerator.Utils.Point, EnemyBehaviour> enemies = new Dictionary<MapGenerator.Utils.Point, EnemyBehaviour>();

    private void Awake()
    {
        if (instance != null)
        {
            throw new UnityException();
        }
        instance = this;
    }

    private void Start()
    {
        if (mapController != null)mapController.ReloadMap();
    }

    public MapCell GetMapCell(int x, int y)
    {
        MapCell cell = mapController.CheckField(x, y);
        return cell;
    }
    public Interactable GetInteractableAt(int x, int y)
    {
        GameObject map_object = mapController.GetObjectAt(x, y);
        return map_object.GetComponent<Interactable>();
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(State);
        if (State == GameState.walk)
        {
            currentAnimationCooldown -= Time.deltaTime;
            if (currentAnimationCooldown < 0)
            {
                currentAnimationCooldown = 0;
                State = GameState.idle;
            }
        }
    }

    public void StartWalking()
    {
        if (State == GameState.idle)
        {
            State = GameState.walk;
            currentAnimationCooldown = AnimationCooldown;
        }
    }
}
