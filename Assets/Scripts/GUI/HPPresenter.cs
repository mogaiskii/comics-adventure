﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HPPresenter : MonoBehaviour
{
    // TODO: Make dynamic
    public GameObject h1;
    public GameObject h2;
    public GameObject h3;
    public GameObject h4;
    public GameObject h5;

    public HeroController player;
    int previousHP = 0;

    private void Awake()
    {
        previousHP = HeroController.MaxHp;
    }
    // Update is called once per frame
    void Update()
    {
        if (player.hp != previousHP)
        {
            switch (player.hp)
            {
                case 5:
                    h1.SetActive(true);
                    h2.SetActive(true);
                    h3.SetActive(true);
                    h4.SetActive(true);
                    h5.SetActive(true);
                    break;
                case 4:
                    h1.SetActive(true);
                    h2.SetActive(true);
                    h3.SetActive(true);
                    h4.SetActive(true);
                    h5.SetActive(false);
                    break;
                case 3:
                    h1.SetActive(true);
                    h2.SetActive(true);
                    h3.SetActive(true);
                    h4.SetActive(false);
                    h5.SetActive(false);
                    break;
                case 2:
                    h1.SetActive(true);
                    h2.SetActive(true);
                    h3.SetActive(false);
                    h4.SetActive(false);
                    h5.SetActive(false);
                    break;
                case 1:
                    h1.SetActive(true);
                    h2.SetActive(false);
                    h3.SetActive(false);
                    h4.SetActive(false);
                    h5.SetActive(false);
                    break;
                case 0:
                    h1.SetActive(false);
                    h2.SetActive(false);
                    h3.SetActive(false);
                    h4.SetActive(false);
                    h5.SetActive(false);
                    break;
            }
            previousHP = player.hp;
        }
    }
}
