﻿using TMPro;
using UnityEngine;

public class KeysPresenter : MonoBehaviour
{
    // TODO: Make dynamic
    public TextMeshProUGUI counter;

    public HeroController player;
    int previousKeys = 0;

    // Update is called once per frame
    void Update()
    {
        if (player.keys != previousKeys)
        {
            counter.text = player.keys.ToString();
            previousKeys = player.keys;
        }
    }
}
