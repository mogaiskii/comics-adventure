﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialoguePanelController : MonoBehaviour
{
    public Sprite EmojiSheet;
    public GameObject HeroEmoji;
    public GameObject OpponentEmoji;
    public GameObject OpponentImage;
    Dialogue dialogue;

    public bool IsInDialogue { get { return dialogue != null; } set { } }

    public void StartDialogue(Dialogue dialogue_)
    {
        if (!IsInDialogue)
            dialogue = dialogue_;
    }

    public void NextDialogueStep()
    {
        
    }
}
