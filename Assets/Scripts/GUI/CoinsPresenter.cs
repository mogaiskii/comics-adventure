﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CoinsPresenter : MonoBehaviour
{
    // TODO: Make dynamic
    public TextMeshProUGUI counter;

    public HeroController player;
    int previousCoins = 0;

    // Update is called once per frame
    void Update()
    {
        if (player.coins != previousCoins)
        {
            counter.text = player.coins.ToString();
            previousCoins = player.coins;
        }
    }
}
