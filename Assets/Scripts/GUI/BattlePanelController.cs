﻿using UnityEngine;
using UnityEngine.UI;

public class BattlePanelController : MonoBehaviour
{
    public GameObject enemyImageBanner;
    public GameObject enemyHint;
    EnemyBehaviour enemy;

    public Slider enemyHpSlider;

    public void Awake()
    {
        // тогда перестает включаться!
        //gameObject.SetActive(false);
    }

    public void setEnemy(EnemyBehaviour enemy_)
    {
        enemy = enemy_;
        setEnemyImage(enemy.image);
        setEnemyHint(enemy.strategy.hint);
    }

    void setEnemyImage(Sprite enemyImage)
    {
        enemyImageBanner.GetComponent<Image>().sprite = enemyImage;
    }
    void setEnemyHint(string hint)
    {
        enemyHint.GetComponent<Text>().text = hint;
    }

    public void Update()
    {
        enemyHpSlider.value = (float)enemy.strategy.hp / (float)enemy.strategy.MaxHP;
    }
}

