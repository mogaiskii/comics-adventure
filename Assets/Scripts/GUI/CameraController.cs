﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    Transform lookTo;
    public float speed = 2f;

    private void Start()
    {
        speed = LevelController.MoveSpeed;
    }

    public void SetLookTo(Transform look_to)
    {
        lookTo = look_to;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (lookTo != null)
        {
            // TODO: Vector3.Slerp
            transform.position = Vector3.MoveTowards(
                transform.position,
                new Vector3(lookTo.position.x, lookTo.position.y, transform.position.z), speed * Time.deltaTime
                );
        }
    }
}
