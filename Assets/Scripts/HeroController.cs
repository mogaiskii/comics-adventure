﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum HeroWalkStates
{
    idle,
    left,
    right,
    up,
    down
}


// TODO: mb via events? Input events (on down, on up, on click, on swipe); Player events (on step, on fight);
public class HeroController : MonoBehaviour
{

    Vector2Int? targetPosition = null;

    public const int MaxHp = 5;

    public int hp { get; private set; } = 5;

    public int coins { get; private set; } = 0;

    public int keys { get; private set; } = 0;

    public HeroWalkStates State { get; private set; } = HeroWalkStates.idle;

    private void Start()
    {
        CameraController camera = Camera.main.GetComponent<CameraController>();
        if (camera != null) camera.SetLookTo(transform);
    }
    private void OnDestroy()
    {
        CameraController camera = Camera.main.GetComponent<CameraController>();
        if (camera != null) camera.SetLookTo(null);
    }

    public void GetProfit(CollectableProfit collectable)
    {
        coins += collectable.coins;
        hp = Mathf.Clamp(hp + collectable.hps, 0, MaxHp);
        keys += collectable.keys;
    }

    // Update is called once per frame
    void Update()
    {
        if (LevelController.instance.State == GameState.idle)
        {
            if (targetPosition.HasValue) JumpToTarget();
            SetNewTarget();
        }
        if (LevelController.instance.State == GameState.walk) GoToTarget();
    }

    void JumpToTarget()
    {
        transform.position = new Vector3(targetPosition.Value.x, targetPosition.Value.y, transform.position.z);
        targetPosition = null;
        State = HeroWalkStates.idle;
    }

    void GoToTarget()
    {
        if (!targetPosition.HasValue) return;

        if (transform.position.x == targetPosition.Value.x && transform.position.y == targetPosition.Value.y)
        {
            targetPosition = null;
            State = HeroWalkStates.idle;
            return;
        }
        Vector3 nextPosition = new Vector3(targetPosition.Value.x, targetPosition.Value.y, transform.position.z);
        transform.position = Vector3.MoveTowards(transform.position, nextPosition, LevelController.MoveSpeed * Time.deltaTime);
    }

    void SetNewTarget()
    {
        if (InputController.swipe.HasValue)
        {
            Vector2Int wantedTarget = new Vector2Int(
                (int)transform.position.x + InputController.swipe.Value.x,
                (int)transform.position.y + InputController.swipe.Value.y
                );
            Debug.Log(wantedTarget);
            if (LevelController.instance.TryTake(wantedTarget.x, wantedTarget.y))
            {
                targetPosition = wantedTarget;
                LevelController.instance.StartWalking();
                if (InputController.swipe.Value.x > 0) { State = HeroWalkStates.right; Debug.Log("right"); }
                else if (InputController.swipe.Value.x < 0) { State = HeroWalkStates.left; Debug.Log("left"); }
                else if (InputController.swipe.Value.y > 0) { State = HeroWalkStates.up; Debug.Log("up"); }
                else if (InputController.swipe.Value.y < 0) { State = HeroWalkStates.down; Debug.Log("down"); }

            }

        }
    }
}
