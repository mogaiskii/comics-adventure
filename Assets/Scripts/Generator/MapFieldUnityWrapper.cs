﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MapGenerator.Utils;
using UnityEngine.Tilemaps;

public class MapFieldUnityWrapper : MonoBehaviour
{
    public FieldTypes type;
    public List<Tile> textures;
}
