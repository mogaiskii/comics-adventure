﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MapGenerator.Utils;

public interface Interactable
{
    void Interact(HeroController player);
    void PreInteract(HeroController player);
}

public class MapCell
{
    public int x { get; private set; }
    public int y { get; private set; }
    public ItemsTypes item_type { get; private set; }
    public FieldTypes field_type { get; private set; }

    public Interactable interactable { get; private set; }

    public MapCell(ItemsTypes itemType, FieldTypes fieldType, int x, int y)
    {
        item_type = itemType;
        field_type = fieldType;
        this.x = x; this.y = y;
        interactable = LevelController.instance.GetInteractableAt(x, y);
    }

    public void SetInteractable(Interactable item)
    {
        interactable = item;
    }
}
