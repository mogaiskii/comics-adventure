﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MapGenerator.Utils;

public class MapItem
{
    public ItemsTypes type { get; private set; }
    List<GameObject> item_variants = new List<GameObject>();

    public MapItem(ItemsTypes type)
    {
        this.type = type;
    }

    public void AddVariant(GameObject item)
    {
        item_variants.Add(item);
    }
    public void RemoveVariant(GameObject item)
    {
        item_variants.Remove(item);
    }

    public GameObject GetGameObject()
    {
        if (item_variants.Count == 0) return null;
        return item_variants[Random.Range(0, item_variants.Count)];
    }
}
