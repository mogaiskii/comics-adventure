﻿namespace MapGenerator.Utils
{

    public enum ItemsTypes
    {
        enemy   = 10,
        chest   = 20,
        diamond = 30,
        heart   = 40,
        key     = 50,
        start   = 60,
        finish  = 70,
    }

    public static class Item
    {
        // public ItemsTypes type { get; private set; }

        public const int MULTIPLIER = 10;
        public const int DIVIDER = 1000;

        public static ItemsTypes getFieldType(int field)
        {
            return (ItemsTypes)(field / MULTIPLIER % DIVIDER * MULTIPLIER);
        }

        public static int UnsetField(int field)
        {
            return field - (int)getFieldType(field);
        }

        public static int SetField(int field, ItemsTypes item)
        {
            return UnsetField(field) + (int)item;
        }
    }

}