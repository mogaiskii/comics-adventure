﻿namespace MapGenerator.Utils
{

    public enum FieldTypes
    {
        wall = 1,
        empty,
        road,
        main_route
    }

    public static class Field
    {
        // public FieldTypes type { get; private set; }

        public const int MULTIPLIER = 1;
        public const int DIVIDER = 10;

        public static FieldTypes getFieldType(int field)
        {
            return (FieldTypes)(field / MULTIPLIER % DIVIDER);
        }
        public static int updateFieldType(int field, FieldTypes value)
        {
            int right = field % MULTIPLIER;
            return field / DIVIDER * DIVIDER + (int)value + right;
        }
    }

}