﻿using MapGenerator.Utils;
using PathFind;
using UnityEngine;
using System.Collections.Generic;

namespace MapGenerator
{
    public class MapGenerator
    {
        List<Leaf> leafs = new List<Leaf>();

        int width;
        int height;
        Leaf root;
        GameMap gameMap;

        public MapGenerator(int width, int height)
        {
            this.width = width;
            this.height = height;
            gameMap = new GameMap(width, height);
            root = new Leaf(0, 0, width, height);
            leafs.Add(root);
        }

        public GameMap Generate()
        {
            bool smth_was_splitted = true;
            while (smth_was_splitted)
            {
                smth_was_splitted = false;

                int leaf_index = 0;
                while (leaf_index < leafs.Count)
                {
                    Leaf item = leafs[leaf_index];
                    if (item.split())
                    {
                        Leaf left = item.leftChild;
                        Leaf right = item.rightChild;
                        if (left != null) leafs.Add(left);
                        if (right != null) leafs.Add(right);
                    }
                    leaf_index++;
                }
            }
            root.CreateRooms();

            List<Room> all_rooms = root.GetAllSubRooms();
            MakePathwayRooms(all_rooms);

            for (int y = 0; y < gameMap.height; y++)
            {
                for (int x = 0; x < gameMap.width; x++)
                {
                    if (Random.Range(0, 10) >= 3) gameMap[y, x] = (int)FieldTypes.empty;
                    else gameMap[y, x] = (int)FieldTypes.wall;
                }
            }

            foreach (Room room in all_rooms) room.Draw(gameMap);

            List<Room> specialRooms = all_rooms.FindAll(
                el => el.roomType == RoomTypes.finish || el.roomType == RoomTypes.start || el.roomType == RoomTypes.key
                );
            foreach (Room room in specialRooms) room.Draw(gameMap);

            CleanSingles(gameMap);
            CleanSingles(gameMap);
            gameMap = CleanUnreachable(gameMap);
            CleanSingles(gameMap);
            // TODO: check - if rooms not collides - collide them!
            DrawWay(gameMap, all_rooms);

            return gameMap;
        }

        void CleanSingles(GameMap gameMap)
        {
            for (int y = 0; y < gameMap.height; y++)
            {
                for (int x = 0; x < gameMap.width; x++)
                {
                    if (Field.getFieldType( gameMap[y,x] ) == FieldTypes.wall) continue;
                    if (Field.getFieldType(gameMap[y, x]) == FieldTypes.road || Field.getFieldType(gameMap[y, x]) == FieldTypes.main_route) continue;

                    if (x > 0 && x < gameMap.width - 1)
                    {
                        if (
                            Field.getFieldType(gameMap[y, x - 1]) == FieldTypes.wall &&
                            Field.getFieldType(gameMap[y, x + 1]) == FieldTypes.wall)
                        {
                            gameMap[y, x] = (int)FieldTypes.wall;
                            continue;
                        }
                    }

                    if (y > 0 && y < gameMap.height - 1)
                    {
                        if (
                            Field.getFieldType(gameMap[y - 1, x]) == FieldTypes.wall &&
                            Field.getFieldType(gameMap[y + 1, x]) == FieldTypes.wall)
                        {
                            gameMap[y, x] = (int)FieldTypes.wall;
                            continue;
                        }
                    }

                    if (x == 0)
                    {
                        if (Field.getFieldType(gameMap[y, 1]) == FieldTypes.wall)
                        {
                            gameMap[y, x] = (int)FieldTypes.wall;
                            continue;
                        }
                    }

                    if (x == gameMap.width - 1)
                    {
                        if (Field.getFieldType(gameMap[y, x - 1]) == FieldTypes.wall)
                        {
                            gameMap[y, x] = (int)FieldTypes.wall;
                            continue;
                        }
                    }



                    if (y == 0)
                    {
                        if (Field.getFieldType(gameMap[1, x]) == FieldTypes.wall)
                        {
                            gameMap[y, x] = (int)FieldTypes.wall;
                            continue;
                        }
                    }

                    if (y == gameMap.height - 1)
                    {
                        if (Field.getFieldType(gameMap[y - 1, x]) == FieldTypes.wall)
                        {
                            gameMap[y, x] = (int)FieldTypes.wall;
                            continue;
                        }
                    }


                }
            }
        }

        GameMap CleanUnreachable(GameMap gameMap)
        {
            Room any_room = root.GetRoom();
            int start_x = any_room.x; int start_y = any_room.y;

            GameMap finalGameMap = new GameMap(gameMap.width, gameMap.height);

            for (int y = 0; y < finalGameMap.height; y++)
            {
                for (int x = 0; x < finalGameMap.width; x++)
                {
                    finalGameMap[y, x] = (int)FieldTypes.wall;
                }
            }

            List<Point> pointsToCheck = new List<Point>();
            Point startPoint = new Point(start_x, start_y);
            pointsToCheck.Add(startPoint);
            List<Point> checkedPoints = new List<Point>();

            while (pointsToCheck.Count > 0)
            {
                Point point = pointsToCheck[0];
                pointsToCheck.RemoveAt(0);
                checkedPoints.Add(point);

                int point_value = gameMap[point.y, point.x];
                if (Field.getFieldType(point_value) == FieldTypes.wall)
                {
                    finalGameMap[point.y, point.x] = (int)FieldTypes.wall;
                    continue;
                }
                    
                if (point.x < gameMap.width - 1) // left avaliable
                {
                    Point newPoint = new Point(point.x + 1, point.y);
                    
                    if (!checkedPoints.Contains(newPoint) && !pointsToCheck.Contains(newPoint))
                        pointsToCheck.Add(newPoint);
                }
                if (point.x > 0) // right available
                {
                    Point newPoint = new Point(point.x - 1, point.y);
                    if (!checkedPoints.Contains(newPoint) && !pointsToCheck.Contains(newPoint))
                        pointsToCheck.Add(newPoint);
                }

                if (point.y < gameMap.height - 1) // bottom available
                {
                    Point newPoint = new Point(point.x, point.y + 1);
                    if (!checkedPoints.Contains(newPoint) && !pointsToCheck.Contains(newPoint))
                        pointsToCheck.Add(newPoint);
                }
                if (point.y > 0) // top available
                {
                    Point newPoint = new Point(point.x, point.y - 1);
                    if (!checkedPoints.Contains(newPoint) && !pointsToCheck.Contains(newPoint))
                        pointsToCheck.Add(newPoint);
                }

                finalGameMap[point.y, point.x] = gameMap[point.y, point.x];
            }
            return finalGameMap;
        }

        void DrawWay(GameMap gameMap, List<Room> all_rooms)
        {
            Room start = all_rooms.Find(el => el.roomType == RoomTypes.start);
            Room finish = all_rooms.Find(el => el.roomType == RoomTypes.finish);

            PathFind.PathFind pathFind = new PathFind.PathFind(gameMap, new FieldTypes[] { FieldTypes.road, FieldTypes.main_route });
            //string ret = gameMap.Print();
            List<Point> way = pathFind.Find(new Point(start.x, start.y), new Point(finish.x, finish.y));
            if (way == null) return;

            foreach (Point point in way)
            {
                gameMap[point.y, point.x] = Field.updateFieldType(gameMap[point.y, point.x], FieldTypes.main_route);
            }
        }

        void MakePathwayRooms(List<Room> all_rooms)
        {
            int rooms_count = all_rooms.Count;

            int min_x_y = -1;
            Room min_room = null;

            int max_x_y = -1;
            Room max_room = null;

            Room random_room = null;

            List<Room> real_rooms = new List<Room>();
            foreach (Room room in all_rooms) if (room.roomType != RoomTypes.hall) real_rooms.Add(room);

            foreach (Room room in real_rooms)
            {
                if (min_x_y == -1)
                {
                    min_x_y = room.x + room.y;
                    min_room = room;
                    max_x_y = room.x + room.y;
                    max_room = room;
                }

                if (room.x + room.y < min_x_y)
                {
                    min_x_y = room.x + room.y;
                    min_room = room;
                }

                if (room.x + room.y > max_x_y)
                {
                    max_x_y = room.x + room.y;
                    max_room = room;
                }
            }

            min_room.SetStartRoom();
            max_room.SetFinishRoom();

            List<Room> less_rooms = new List<Room>();
            foreach (Room room in real_rooms) if (room.roomType == RoomTypes.simple) less_rooms.Add(room);

            random_room = less_rooms[Random.Range(0, less_rooms.Count)];
            random_room.SetKeyRoom();
        }
    }
}
