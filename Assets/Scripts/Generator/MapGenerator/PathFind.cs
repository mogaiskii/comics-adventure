﻿using System;
using System.Collections.Generic;
using MapGenerator.Utils;


namespace PathFind
{

    public class PathFind
    {
        Dictionary<(int, int), Point> previouses = new Dictionary<(int, int), Point>();
        GameMap gameMap;
        FieldTypes[] validValues;

        public delegate Point ChooseNodeStrategy(List<Point> nodes);
        ChooseNodeStrategy chooseNodeStrategy = null;

        int GetGameMapHeight()
        {
            return gameMap.height;
        }
        int GetGameMapWidth()
        {
            return gameMap.width;
        }

        public PathFind(GameMap gameMap, FieldTypes[] valid_values, ChooseNodeStrategy chooseNodeStrategy = null)
        {
            this.gameMap = gameMap;
            this.validValues = valid_values;
            if (chooseNodeStrategy != null)
            {
                this.chooseNodeStrategy = chooseNodeStrategy;
            }
        }

        Point ChooseNode(List<Point> nodes)
        {
            if (chooseNodeStrategy != null)
            {
                return chooseNodeStrategy.Invoke(nodes);
            }
            return nodes[0];
        }

        bool IsReachable(Point node)
        {
            if (node.x < 0 || node.y < 0 || node.y > GetGameMapHeight() || node.x > GetGameMapWidth())
            {
                return false;
            }
            
            FieldTypes fieldType = Field.getFieldType(gameMap[node.y, node.x]);
            foreach (var item in validValues)
            {
                if (item == fieldType) return true;
            }

            return false;
        }

        List<Point> GetAdjNodes(Point node)
        {
            List<Point> adj_moves = new List<Point>() { 
                new Point(-1, 0), // left
                new Point(1, 0), // right
                new Point(0, -1), // top
                new Point(0, 1) // down
            };
            List<Point> adj_points = new List<Point>();
            foreach (Point move in adj_moves)
            {
                Point new_point = new Point(move.x + node.x, move.y + node.y);
                if (IsReachable(new_point))
                {
                    adj_points.Add(new_point);
                }
            }
            return adj_points;
        }

        public List<Point> Find(Point start, Point finish)
        {
            List<Point> reachable = new List<Point>() { start };
            List<Point> explored = new List<Point>();
            while (reachable.Count > 0)
            {
                Point node = ChooseNode(reachable);
                if (node.x == finish.x && node.y == finish.y)
                {
                    return BuildPath(node);
                }

                reachable.Remove(node);
                explored.Add(node);

                List<Point> adj_points = GetAdjNodes(node);

                foreach (Point adj_item in adj_points)
                {
                    if (!explored.Contains(adj_item) && !reachable.Contains(adj_item))
                    {
                        previouses.Add((adj_item.x, adj_item.y), node);
                        reachable.Add(adj_item);
                    }
                }
            }
            return null;
        }
        List<Point> BuildPath(Point node)
        {
            List<Point> path = new List<Point>();
            while (true)
            {
                path.Add(node);
                if (!previouses.ContainsKey((node.x, node.y)))
                {
                    return path;
                }
                node = previouses[(node.x, node.y)];
            }

        }
    }

}