﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MapGenerator.Utils
{

    public class Leaf
    {
        public int absolute_min_leaf_size { get; private set; } = 4;
        public int absolute_max_leaf_size { get; private set; } = 7;
        public float rel_min_leaf_size { get; private set; } = 1.3f;
        
        int width;
        int height;
        int x;
        int y;

        public Leaf leftChild { get; private set; }
        public Leaf rightChild { get; private set; }
        bool wouldIgnoreSplitting = false;

        public Room room { get; private set; }

        public List<Room> halls { get; private set; }  = new List<Room>();

        public Leaf(int x, int y, int width, int height)
        {
            this.x = x; this.y = y; this.width = width; this.height = height;
        }
        public Leaf(int x, int y, int width, int height, int absolute_min_leaf_size, int absolute_absolute_max_leaf_size, float rel_min_leaf_size)
        {
            this.x = x; this.y = y; this.width = width; this.height = height;
            this.absolute_min_leaf_size = absolute_min_leaf_size;
            this.absolute_max_leaf_size = absolute_absolute_max_leaf_size;
            this.rel_min_leaf_size = rel_min_leaf_size;
        }

        public bool split()
        {
            if (!IsLeafChild()) return false;
            if (height <= absolute_min_leaf_size * 2 && width <= absolute_min_leaf_size * 2) return false;
            if (wouldIgnoreSplitting == false && height <= absolute_max_leaf_size && width <= absolute_max_leaf_size) wouldIgnoreSplitting = Random.Range(0, 9) >= 5;
            if (wouldIgnoreSplitting) return false;

            bool splitHorizontal = false;
            if (height <= absolute_min_leaf_size * 2) splitHorizontal = true;
            else if (width / height > rel_min_leaf_size) splitHorizontal = true;
            else if (width > absolute_min_leaf_size * 2 && height / width <= rel_min_leaf_size) splitHorizontal = Random.Range(0, 9) >= 5;

            int maxSplitPoint;
            if (splitHorizontal) maxSplitPoint = width - absolute_min_leaf_size;
            else maxSplitPoint = height - absolute_min_leaf_size;

            int splitPoint;
            if (maxSplitPoint == absolute_min_leaf_size) splitPoint = absolute_min_leaf_size;
            else splitPoint = Random.Range(absolute_min_leaf_size, maxSplitPoint);

            if (splitHorizontal)
            {
                leftChild = new Leaf(x, y, splitPoint, height);
                rightChild = new Leaf(x + splitPoint, y, width - splitPoint, height);
            } else
            {
                leftChild = new Leaf(x, y, width, splitPoint);
                rightChild = new Leaf(x, y + splitPoint, width, height - splitPoint);
            }

            return true;
        }

        bool IsLeafChild()
        {
            return leftChild == null && rightChild == null;
        }

        public Room GetRoom()
        {
            if (room != null) return room;
            Room lRoom = null; Room rRoom = null;
            if (leftChild != null) lRoom = leftChild.GetRoom();
            if (rightChild != null) rRoom = rightChild.GetRoom();

            if (leftChild == null && rightChild == null) return null;

            if (rRoom == null) return lRoom;
            if (lRoom == null) return rRoom;

            if (Random.Range(0, 9) >= 5) return lRoom;
            return rRoom;
        }

        // TODO: Help me ! There is no tail-recursion optimization in C#!
        public List<Room> GetAllSubRooms()
        {
            List<Room> rooms = new List<Room>();

            if (room != null) rooms.Add(room);
            if (halls != null && halls.Count > 0) rooms.AddRange(halls);

            if (leftChild != null) rooms.AddRange(leftChild.GetAllSubRooms());
            if (rightChild != null) rooms.AddRange(rightChild.GetAllSubRooms());

            return rooms;
        }

        public void CreateRooms()
        {
            if (!IsLeafChild())
            {
                if (leftChild != null) leftChild.CreateRooms();
                if (rightChild != null) rightChild.CreateRooms();
                if (leftChild != null && rightChild != null) halls = CreateHall(leftChild.GetRoom(), rightChild.GetRoom());

            } else
            {
                int room_width = absolute_min_leaf_size - 2;
                if (width > absolute_min_leaf_size) room_width = Random.Range(absolute_min_leaf_size - 2, width - 1);

                int room_height = absolute_min_leaf_size - 2;
                if (height > absolute_min_leaf_size) room_height = Random.Range(absolute_min_leaf_size - 2, height - 1);

                int room_x = x + 1;
                if (width - room_width > 1) room_x = x + Random.Range(1, width - room_width - 1);

                int room_y = x + 1;
                if (height - room_height > 1) room_y = y + Random.Range(1, height - room_height - 1);

                room = new Room(room_x, room_y, room_width, room_height, FieldTypes.road, RoomTypes.simple);
            }
        }

        List<Room> CreateHall(Room left, Room right)
        {
            Point point_1 = new Point(
                Random.Range(left.x + 1, left.x + left.width - 1),
                Random.Range(left.y + 1, left.y + left.height - 1)
                );
            Point point_2 = new Point(
                Random.Range(right.x + 1, right.x + right.width - 1),
                Random.Range(right.y + 1, right.y + right.height - 1)
                );
            int delta_x = point_2.x - point_1.x;
            int delta_y = point_2.y - point_1.y;

            int abs_delta_x = Mathf.Abs(delta_x);
            int abs_delta_y = Mathf.Abs(delta_y);
            int hall1_width = Random.Range(1, 3); // TODO: MAGIC!
            int hall2_width = Random.Range(1, 3); // TODO: MAGIC!
            bool horizontal_first = Random.Range(0, 9) >= 5;

            if (delta_x < 0)
            {
                if (delta_y < 0)
                {
                    if (horizontal_first)
                    {
                        halls.Add(new Room(point_2.x, point_1.y, abs_delta_x, hall1_width, FieldTypes.road));
                        halls.Add(new Room(point_2.x, point_2.y, hall2_width, abs_delta_y, FieldTypes.road));
                    } else
                    {
                        halls.Add(new Room(point_2.x, point_2.y, abs_delta_x, hall1_width, FieldTypes.road));
                        halls.Add(new Room(point_1.x, point_2.y, hall2_width, abs_delta_y, FieldTypes.road));
                    }
                } else if (delta_y > 0)
                {
                    if (horizontal_first)
                    {
                        halls.Add(new Room(point_2.x, point_1.y, abs_delta_x, hall1_width, FieldTypes.road));
                        halls.Add(new Room(point_2.x, point_1.y, hall2_width, abs_delta_y, FieldTypes.road));
                    }
                    else
                    {
                        halls.Add(new Room(point_2.x, point_2.y, abs_delta_x, hall1_width, FieldTypes.road));
                        halls.Add(new Room(point_1.x, point_1.y, hall2_width, abs_delta_y, FieldTypes.road));
                    }
                } else
                {
                    halls.Add(new Room(point_2.x, point_2.y, abs_delta_x, hall1_width, FieldTypes.road));
                }
            } else if (delta_x > 0)
            {
                if (delta_y < 0)
                {
                    if (horizontal_first)
                    {
                        halls.Add(new Room(point_1.x, point_2.y, abs_delta_x, hall1_width, FieldTypes.road));
                        halls.Add(new Room(point_1.x, point_2.y, hall2_width, abs_delta_y, FieldTypes.road));
                    }
                    else
                    {
                        halls.Add(new Room(point_1.x, point_1.y, abs_delta_x, hall1_width, FieldTypes.road));
                        halls.Add(new Room(point_2.x, point_2.y, hall2_width, abs_delta_y, FieldTypes.road));
                    }
                }
                else if (delta_y > 0)
                {
                    if (horizontal_first)
                    {
                        halls.Add(new Room(point_1.x, point_1.y, abs_delta_x, hall1_width, FieldTypes.road));
                        halls.Add(new Room(point_2.x, point_1.y, hall2_width, abs_delta_y, FieldTypes.road));
                    }
                    else
                    {
                        halls.Add(new Room(point_1.x, point_2.y, abs_delta_x, hall1_width, FieldTypes.road));
                        halls.Add(new Room(point_1.x, point_1.y, hall2_width, abs_delta_y, FieldTypes.road));
                    }
                }
                else
                {
                    halls.Add(new Room(point_1.x, point_1.y, abs_delta_x, hall1_width, FieldTypes.road));
                }
            } else
            {
                if (delta_y  < 0)
                {
                    halls.Add(new Room(point_2.x, point_2.y, hall2_width, abs_delta_y, FieldTypes.road));
                } else if (delta_y > 0)
                {
                    halls.Add(new Room(point_1.x, point_1.y, hall2_width, abs_delta_y, FieldTypes.road));
                }
            }

            return halls;
        }

    }

}
