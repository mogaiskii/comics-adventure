﻿using UnityEngine;

namespace MapGenerator.Utils
{
    public enum RoomTypes
    {
        simple,
        key,
        start,
        finish,
        hall
    }

    public class Room
    {
        public static readonly ItemsTypes[] availRandomItemTypes = { ItemsTypes.chest, ItemsTypes.diamond, ItemsTypes.enemy };

        public int width { get; private set; }
        public int height { get; private set; }
        public int x { get; private set; }
        public int y { get; private set; }
        public FieldTypes roomFieldType { get; private set; }
        public ItemsTypes roomItemType { get; private set; } // TODO: FIXME: комната может содержать несколько итемов
        public RoomTypes roomType { get; private set; }

        public Room(int x, int y, int width, int height, FieldTypes room_field_type, RoomTypes room_type = RoomTypes.simple)
        {
            this.x = x; this.y = y; this.width = width; this.height = height;
            roomFieldType = room_field_type;
            roomType = room_type;

            if (availRandomItemTypes.Length > 0)
                roomItemType = availRandomItemTypes[Random.Range(0, availRandomItemTypes.Length)];
        }

        public void Draw(GameMap gameMap)
        {
            int self_width = width;
            if (x + self_width >= gameMap.width) self_width = gameMap.width - x;

            int self_height = height;
            if (y + self_height >= gameMap.height) self_height = gameMap.height - y;

            GameMap self_map = new GameMap(self_width, self_height);
            for (int y_ = 0; y_ < self_height; y_++)
            {
                for (int x_ = 0; x_ < self_width; x_++)
                {
                    self_map[y_, x_] = (int)roomFieldType;
                }
            }

            switch (roomType)
            {
                case RoomTypes.start:
                    self_map[0, 0] += (int)ItemsTypes.start;
                    break;
                case RoomTypes.finish:
                    self_map[self_height - 1, self_width - 1] += (int)ItemsTypes.finish;
                    break;
                default:
                    self_map[self_height / 2, self_width / 2] += (int)roomItemType;
                    break;
            }

            // TODO: FIXME: можно без этого цикла обойтись. только с первым.
            for (int y_ = 0; y_ < self_height; y_++)
            {
                for (int x_ = 0; x_ < self_width; x_++)
                {
                    gameMap[y_+y, x_+x] = self_map[y_,x_];
                }
            }
        }
    
        public void SetKeyRoom()
        {
            roomType = RoomTypes.key;
            roomItemType = ItemsTypes.key;

#if UNITY_EDITOR
            // TODO: if debug only
            roomFieldType = FieldTypes.main_route;
#endif

        }

        public void SetStartRoom()
        {
            roomType = RoomTypes.start;
            roomItemType = ItemsTypes.start;
            roomFieldType = FieldTypes.main_route;
        }

        public void SetFinishRoom()
        {
            roomType = RoomTypes.finish;
            roomItemType = ItemsTypes.finish;
            roomFieldType = FieldTypes.main_route;
        }

        public void SetRoomItem(ItemsTypes item)
        {
            // TOOD: raise exc
            if (item == ItemsTypes.key || item == ItemsTypes.finish || item == ItemsTypes.start) return;

            roomItemType = item;
        }

    }

}