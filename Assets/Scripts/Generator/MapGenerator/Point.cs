﻿
namespace MapGenerator.Utils
{
    public struct Point
    {
        public int x; public int y;
        public Point(int x, int y)
        {
            this.x = x; this.y = y;
        }
        public override bool Equals(object obj)
        {
            var item = (Point)obj;

            if (item.x == this.x && item.y == this.y) return true;
            return false;
        }

        public override int GetHashCode()
        {
            var hashCode = 1502939027;
            hashCode = hashCode * -1521134295 + x.GetHashCode();
            hashCode = hashCode * -1521134295 + y.GetHashCode();
            return hashCode;
        }
    }
}