﻿using System.Collections.Generic;


namespace MapGenerator.Utils
{
    // TODO: Rename me
    public class GameMap
    {
        int[,] game_map;
        public int width { get; private set; }
        public int height { get; private set; }

        // TODO: костыль
        bool reverted = false;
        public void Revert() { reverted = !reverted; }

        public GameMap(int width, int height)
        {
            this.width = width; this.height = height;
            game_map = new int[height, width];
        }
        public string Print()
        {
            string all = "";
            for (int y = 0; y < height; y++)
            {

                string line = "";
                for (int x = 0; x < width; x++)
                {
                    line += game_map[y, x].ToString();
                    if (x != width - 1) line += ",";
                }
                all += line + "\n";
            }
            return all;
        }

        public int this[int y, int x]
        {
            get
            {
                return game_map[y, x];
            }
            set
            {
                game_map[y, x] = value;
            }
        }
    }

}