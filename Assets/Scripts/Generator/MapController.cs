﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MapGenerator.Utils;
using UnityEngine.Tilemaps;
using System;


public class MapController : MonoBehaviour
{
    // TODO: someting should set this
    public int width = 20;
    public int height = 30;

    public readonly int z_index = 0;

    public List<MapFieldUnityWrapper> load_fields = new List<MapFieldUnityWrapper>();
    public List<MapItemUnityWrapper> load_items = new List<MapItemUnityWrapper>();
    public Transform itemContainer;

    public Tilemap tilemap = null;

    Dictionary<ItemsTypes, MapItem> itemsHash = new Dictionary<ItemsTypes, MapItem>();
    Dictionary<FieldTypes, MapField> fieldsHash = new Dictionary<FieldTypes, MapField>();

    Dictionary<Point, GameObject> objectsRegistry = new Dictionary<Point, GameObject>();

    GameMap gameMap;

    private void Awake()
    {
        foreach (var item in load_fields)
        {
            if (!fieldsHash.ContainsKey(item.type))
            {
                fieldsHash.Add(item.type, new MapField(item.type));
            }
            foreach (var tile in item.textures)
            {
                fieldsHash[item.type].AddVariant(tile);
            }
        }
        foreach (var item in load_items)
        {
            if (!itemsHash.ContainsKey(item.type))
            {
                itemsHash.Add(item.type, new MapItem(item.type));
            }
            foreach (var tile in item.items)
            {
                itemsHash[item.type].AddVariant(tile);
            }
        }
    }

    public GameMap ReloadMap()
    {
        objectsRegistry.Clear();
        for (int i = 0; i < itemContainer.childCount; i++)
        {
            GameObject child = itemContainer.GetChild(i).gameObject;
            Destroy(child);
        }
        itemContainer.DetachChildren();

        gameMap = new MapGenerator.MapGenerator(width, height).Generate();

        LoadTiles();
        LoadItems();

        return gameMap;
    }

    public MapCell CheckField(int x, int y)
    {
        int map_x = x; int map_y = y;

        ItemsTypes item; FieldTypes field;

        if (map_x > width || map_x < 0) field = FieldTypes.wall;
        else if (map_y > height || map_y < 0) field = FieldTypes.wall;
        else field = Field.getFieldType(gameMap[map_y, map_x]);

        item = Item.getFieldType(gameMap[map_y, map_x]);
        MapCell map_cell = new MapCell(item, field, map_x, map_y);
        return map_cell;
    }

    public GameObject GetObjectAt(int x, int y)
    {
        return objectsRegistry[new Point(x, y)];
    }
    public GameObject MoveMapItem(int old_x, int old_y, int new_x, int new_y)
    {
        GameObject gameObject = objectsRegistry[new Point(old_x, old_y)];
        if (gameObject == null) return null;

        ItemsTypes item_type = Item.getFieldType(gameMap[old_y, old_x]);
        gameMap[old_y, old_x] = Item.UnsetField(gameMap[old_y, old_x]);
        gameMap[new_y, new_x] = Item.SetField(gameMap[new_y, new_x], item_type);

        objectsRegistry.Remove(new Point(old_x, old_y));
        objectsRegistry.Add(new Point(new_x, new_y), gameObject);
        return gameObject;
    }
    public GameObject DeleteMapItem(int x, int y)
    {
        GameObject gameObject = objectsRegistry[new Point(x, y)];
        if (gameObject == null) return null;
        gameMap[y, x] = Item.UnsetField(gameMap[y, x]);
        objectsRegistry.Remove(new Point(x, y));
        return gameObject;
    }

    void LoadTiles()
    {
        if (tilemap == null) return;

        int width_shift = 0;//width / 2;
        int height_shift = 0;//height / 2;
        for (int map_y = 0; map_y < height; map_y++)
        {
            int y = map_y - height_shift;
            for (int map_x = 0; map_x < width; map_x++)
            {
                int x = map_x - width_shift;
                MapField field;
                if (fieldsHash.TryGetValue(Field.getFieldType(gameMap[map_y, map_x]), out field))
                {
                    // TODO: What to do with wall??
                    tilemap.SetTile(new Vector3Int(x, y, z_index), field.GetTile());
                }

            }
        }

        if (fieldsHash.ContainsKey(FieldTypes.wall))
        {
            MapField wallField = fieldsHash[FieldTypes.wall];
            for (int x = 0; x < width + 2; x++)
            {
                int real_x = x - width_shift - 1;
                int real_y_1 = -height_shift - 1;
                int real_y_2 = height_shift;

                tilemap.SetTile(new Vector3Int(real_x, real_y_1, z_index), wallField.GetTile());
                tilemap.SetTile(new Vector3Int(real_x, real_y_2, z_index), wallField.GetTile());
            }
            for (int y = 0; y < height + 2; y++)
            {
                int real_y = y - height_shift - 1;
                int real_x_1 = -width_shift - 1;
                int real_x_2 = width_shift;
                tilemap.SetTile(new Vector3Int(real_x_1, real_y, z_index), wallField.GetTile());
                tilemap.SetTile(new Vector3Int(real_x_2, real_y, z_index), wallField.GetTile());
            }
        }

    }

    void LoadItems()
    {
        int width_shift = 0;// width / 2;
        int height_shift = 0;//height / 2;
        for (int map_y = 0; map_y < height; map_y++)
        {
            int y = map_y - height_shift;
            for (int map_x = 0; map_x < width; map_x++)
            {
                int x = map_x - width_shift;
                int map_value = gameMap[map_y, map_x];
                ItemsTypes itemType = Item.getFieldType(map_value);
                MapItem item;
                if (itemsHash.TryGetValue(itemType, out item))
                {
                    // TODO: Mb change
                    GameObject newItem = Instantiate(item.GetGameObject(), new Vector3Int(x, y, z_index - 1), Quaternion.identity, itemContainer);
                    objectsRegistry.Add(new Point(x, y), newItem);
                }

            }
        }
    }
}
