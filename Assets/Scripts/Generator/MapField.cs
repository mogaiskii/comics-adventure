﻿using System.Collections.Generic;
using UnityEngine;
using MapGenerator.Utils;
using UnityEngine.Tilemaps;

public class MapField
{
    public FieldTypes type { get; private set; }
    List<Tile> item_variants = new List<Tile>();

    public MapField(FieldTypes type)
    {
        this.type = type;
    }

    public void AddVariant(Tile item)
    {
        item_variants.Add(item);
    }
    public void RemoveVariant(Tile item)
    {
        item_variants.Remove(item);
    }

    public Tile GetTile()
    {
        if (item_variants.Count == 0) return null;
        int index = Random.Range(0, item_variants.Count);
        return item_variants[index];
    }

}
