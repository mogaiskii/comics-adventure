﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MapGenerator.Utils;
using UnityEngine.Tilemaps;

public class MapItemUnityWrapper : MonoBehaviour
{
    public ItemsTypes type;
    public List<GameObject> items;
}
