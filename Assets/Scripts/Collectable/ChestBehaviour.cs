﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestBehaviour : CollectableBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        profit = new CollectableProfit
        {
            coins = 10,
            hps = 1
        };
    }
}
