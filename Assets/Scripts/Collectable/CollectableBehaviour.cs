﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public struct CollectableProfit
{
    public int coins;
    public int hps;
    public int keys;
    // List<EquipItems> toEquip;

    public CollectableProfit(int coins = 0, int hps = 0, int keys = 0)
    {
        this.coins = coins; this.hps = hps; this.keys = keys;
    }
}

public class CollectableBehaviour : MonoBehaviour, Interactable
{
    protected CollectableProfit profit;

    private void Start()
    {
        profit = new CollectableProfit
        {
            coins = 0,
            hps = 0
        };
    }

    public CollectableProfit Collect()
    {
        Destroy(gameObject);
        return profit;
    }
    public void Interact(HeroController hero)
    {
        hero.GetProfit(profit);
    }

    public void PreInteract(HeroController player)
    {
    }
}
