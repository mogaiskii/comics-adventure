﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class DoorController : MonoBehaviour
{
    //private void OnGUI()
    //{
    //    DisplayLevel(TargetScene);
    //}

    //public void DisplayLevel(SceneReference scene)
    //{
    //    GUILayout.Label(new GUIContent("Scene name Path: " + scene));
    //    if (GUILayout.Button("Load " + scene))
    //    {
    //        UnityEngine.SceneManagement.SceneManager.LoadScene(scene);
    //    }
    //}

    //TODO: Make required for build
    public SceneReference TargetScene;
    public bool isEnabled = true;

    public bool GoToScene(HeroController player)
    {
        if (player.keys > 0 && TargetScene != null && isEnabled)
        {
            //TODO: player.removeOneKey()
            //TODO: сделать по человечески!!!!!
            SceneManager.LoadScene(TargetScene.ScenePath);
            return true;
        }
        return false;
    }
}
