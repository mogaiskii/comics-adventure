﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: вынести в common либу!
sealed public class InputController : MonoBehaviour
{
    public static float MAGIC_OFFSET { get; private set; } = 7f;
    public static Vector2Int? swipe { get; private set; } = null;
    Vector2? touchStart = null; // for swipe tracking

    // есть ли сейчас зажатие любого рода? (тач/лкм.мышь)
    public static bool GetIsDown()
    {
#if (UNITY_EDITOR || UNITY_STANDALONE)
        return Input.GetMouseButton(0);
#elif (UNITY_ANDROID || UNITY_IOS)
        return Input.touchCount > 0;
#endif
        throw new NotImplementedException("GetIsDown is only for mobile/desktop");
    }

    // было ли только что нажатие? (начало тача/только что нажали на лкм.мышь)
    public static bool GetClickDown()
    {
#if (UNITY_EDITOR || UNITY_STANDALONE)
        return Input.GetMouseButtonDown(0);
#elif (UNITY_ANDROID || UNITY_IOS)
        if (GetIsDown())
            return Input.GetTouch(0).phase == TouchPhase.Began;
        else return false;
#endif
        throw new NotImplementedException("GetClickDown is only for mobile/desktop");
    }

    // было ли только что отжатие? (конец тача/только что отпустили лкм.мышь)
    public static bool GetClickUp()
    {
#if (UNITY_EDITOR || UNITY_STANDALONE)
        return Input.GetMouseButtonDown(0);
#elif (UNITY_ANDROID || UNITY_IOS)
        if (GetIsDown())
            return Input.GetTouch(0).phase == TouchPhase.Ended;
        else return false;
#endif
        throw new NotImplementedException("GetClickUp is only for mobile/desktop");
    }

    /*
     если есть зажатие - координаты зажатия на экране
     если нет - null
         */

    public static Vector2? GetPosition()
    {
#if (UNITY_EDITOR || UNITY_STANDALONE)
        return Input.mousePosition;
#elif (UNITY_ANDROID || UNITY_IOS)
        if (GetIsDown())
            return Input.GetTouch(0).position;
        else return null;
#endif
        throw new NotImplementedException("GetPosition is only for mobile/desktop");
    }

    private void Update()
    {
        swipe = null;

        if (GetIsDown())
        {
            Vector2 newPress = GetPosition().Value;
            if (touchStart.HasValue &&
                (Mathf.Abs(touchStart.Value.x - newPress.x) + Mathf.Abs(touchStart.Value.y - newPress.y)) > MAGIC_OFFSET
                ) // means swipe
            {
                if (Mathf.Abs(touchStart.Value.x - newPress.x) > Mathf.Abs(touchStart.Value.y - newPress.y)) // hor swipe
                {
                    if (touchStart.Value.x > newPress.x) // to left
                    {
                        swipe = new Vector2Int(-1, 0);
                    }
                    else // to right
                    {
                        swipe = new Vector2Int(1, 0);
                    }
                }
                else // vert swipe
                {
                    if (touchStart.Value.y > newPress.y) // to up
                    {
                        swipe = new Vector2Int(0, -1);
                    }
                    else // to down
                    {
                        swipe = new Vector2Int(0, 1);
                    }
                }
            } else // touch start
            {
                touchStart = newPress;
            }
        } else
        {
            touchStart = null;
        }
    }
}
