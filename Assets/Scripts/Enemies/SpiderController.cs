﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UIElements;

public class SpiderController : EnemyBehaviour
{
    private void Awake()
    {
        strategy = new EnemyStrategy("SWIPE", BattleAction.swipe, 5, 2, 3, 2);
    }
}
