﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public struct ValidDirections
{
    public bool left; public bool right; public bool up; public bool down;
}

enum Directions
{
    left,
    right,
    up,
    down,
    stop
}


// !!! NOTE !!!
// Needs IsTrigger=true on collider!
public class EnemyBehaviour : MonoBehaviour, Interactable
{
    float MAX_VISIBILITY = 10f;
    float CLOSE_VISIBILITY = 4f;

    protected Vector3? targetPosition = null;
    public int distance = 1;
    public Sprite image;
    public EnemyStrategy strategy { get; protected set; }
    bool stop = false;

    Vector3 colliderOffset;

    public void Interact(HeroController hero)
    {
    }
    public void PreInteract(HeroController player)
    {
    }

    private void Start()
    {
        Collider2D collider = GetComponent<Collider2D>();
        colliderOffset = new Vector3(collider.offset.x, collider.offset.y, 0);
    }
    public void Battle()
    {
        stop = true;
    }


    void TargetViaMoveCloser(Vector3 playerPosition)
    {
        // TODO: valid directions!!
        Vector2 targetDirection = Vector2.zero;


        if (Mathf.Abs(transform.position.x - playerPosition.x) > Mathf.Abs(transform.position.y - playerPosition.y)) // идем по горизонтали
        {
            if (transform.position.x < playerPosition.x)
            {
                targetDirection = new Vector2(1, 0);
            } else
            {
                targetDirection = new Vector2(-1, 0);
            }
        } else
        {
            if (transform.position.y < playerPosition.y)
            {
                targetDirection = new Vector2(0, 1);
            }
            else
            {
                targetDirection = new Vector2(0, -1);
            }
        }
    }
    void TargetViaRandom()
    {

        ArrayList availableDirections = new ArrayList();
        Debug.Log(availableDirections);
        availableDirections.Add(Directions.stop);
        //if (directions.left) availableDirections.Add(Directions.left);
        //if (directions.right) availableDirections.Add(Directions.right);
        //if (directions.up) availableDirections.Add(Directions.up);
        //if (directions.down) availableDirections.Add(Directions.down);

        int directionIndex = Random.Range(0, availableDirections.Count);
        Directions direction = (Directions)availableDirections[directionIndex];

        Vector2 targetDirection = Vector2.zero;

        switch (direction)
        {
            case Directions.right:
                targetDirection = new Vector2(1, 0);
                break;
            case Directions.left:
                targetDirection = new Vector2(-1, 0);
                break;
            case Directions.up:
                targetDirection = new Vector2(0, 1);
                break;
            case Directions.down:
                targetDirection = new Vector2(0, -1);
                break;
        }
        //targetPosition = new Vector3(
        //        transform.position.x + targetDirection.x * distance * LevelController.MAGIC_CELL,
        //        transform.position.y + targetDirection.y * distance * LevelController.MAGIC_CELL,
        //        transform.position.z
        //        );
        //checkTargetPosition();
    }



    // Update is called once per frame
    void Update()
    {
        if (!stop && targetPosition.HasValue)
        {
            GoToTarget();
        }
        // TODO: if stop - stop animation
    }

    void GoToTarget()
    {
        
        if (targetPosition == transform.position)
        {
            targetPosition = null;
            return;
        }
        transform.position = Vector3.MoveTowards(transform.position, targetPosition.Value, LevelController.MoveSpeed * distance * Time.deltaTime);
    }
}
