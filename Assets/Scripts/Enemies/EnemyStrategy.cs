﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum BattleAction
{
    tap,
    swipe,
    swipeVertical,
    swipeLeft,
    swipeRight,
    swipeHorizontal,
    swipeUp,
    swipeDown,
    nothing
}

public enum BattleStatus
{
    nothing,
    going,
    end
}

public class EnemyStrategy
{
    public string hint { get; private set; }
    BattleAction action;
    public int MaxHP { get; protected set; }
    public int hp { get; protected set; }
    bool wasSwipe = false;

    float minCD;
    float maxCD;
    float timeSincePast; // время с момента прошлого удара
    int damage;

    public EnemyStrategy(string hint_, BattleAction action_, int hp_, float minCD_, float maxCD_, int damage_)
    {
        hint = hint_;
        action = action_;
        hp = hp_;
        MaxHP = hp_;
        minCD = minCD_;
        maxCD = maxCD_;
        damage = damage_;
    }

    public BattleStatus Update(float deltaTime)
    {
        timeSincePast += deltaTime;
        if (timeSincePast > maxCD)
        {
            //LevelController.HitThePlayer(damage);
            timeSincePast = 0;
        } else if (timeSincePast > minCD)
        {
            bool shouldHit = (Random.Range(0, 3) == 0);
            if (shouldHit)
            {
                //LevelController.HitThePlayer(damage);
                timeSincePast = 0;
            }
        }

        BattleStatus status = BattleStatus.nothing;

        if (action == BattleAction.tap)
        {
            if (InputController.GetClickDown())
            {
                hp--;
                status = BattleStatus.going;
            }
        }

        if (action == BattleAction.swipe)
        {
            if (InputController.swipe.HasValue && !wasSwipe)
            {
                wasSwipe = true;
                hp--;
                status = BattleStatus.going;
            }
        }

        if (hp <= 0)
        {
            status = BattleStatus.end;
        }

        if (!InputController.swipe.HasValue) wasSwipe = false;

        return status;
    }
}
